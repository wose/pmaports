# Forked from Alpine to apply Purism's patches for responsivness
pkgname=gtk+3.0
pkgver=9999_git20200616
pkgrel=2
_commit="21ecddab70544dc4421cd34247e9c7c2ef551409"
pkgdesc="The GTK+ Toolkit (v3)"
url="https://www.gtk.org/"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.post-deinstall"
arch="all"
license="LGPL-2.1-or-later"
options="!check" # Most glade tests fail :c
subpackages="$pkgname-demo $pkgname-dev $pkgname-lang $pkgname-dbg"
depends="shared-mime-info gtk-update-icon-cache"

replaces="gtk+"

depends_dev="
	atk-dev
	gdk-pixbuf-dev
	glib-dev
	libepoxy-dev
	libxext-dev
	libxi-dev
	libxinerama-dev
	wayland-protocols
	wayland-libs-client
	wayland-libs-cursor
	libxkbcommon-dev
	"
makedepends="
	$depends_dev
	cups-dev
	expat-dev
	gettext-dev
	gnutls-dev
	gobject-introspection-dev
	libice-dev
	tiff-dev
	zlib-dev
	at-spi2-atk-dev
	cairo-dev
	fontconfig-dev
	pango-dev
	wayland-dev
	libx11-dev
	libxcomposite-dev
	libxcursor-dev
	libxdamage-dev
	libxfixes-dev
	libxrandr-dev
	meson
	gtk-doc
	iso-codes-dev
	"
checkdepends="
	xvfb-run
	ibus
	librsvg
	gdk-pixbuf
	"
source="https://source.puri.sm/Librem5/gtk/-/archive/$_commit/gtk-$_commit.tar.gz
	10-Revert-gdkseatdefault-Grab-touch-events-where-applic.patch
	check-version.py
	0001-gtk-meson.build-add-new-hdy-files.patch
	"

builddir="$srcdir/gtk-$_commit"

prepare() {
	default_prepare

	# Prevent unexpected downgrade (pma#694)
	for _ver in gtk_minor_version gtk_micro_version gtk_interface_age; do
		sed -i "s/m4_define(\[$_ver\], \[.*\])/m4_define([$_ver], [99])/g" configure.ac
	done
	sed -i "s/        version: '.*',/        version: '3.99.99',/g" meson.build
	sed -i "s/^gtk_interface_age = .*/gtk_interface_age = 99/g" meson.build

	# Upstream forgot to include this in the tarball
	# https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/1776
	mv "$srcdir"/check-version.py .
}

build() {
	# postmarketOS specific: disable stuff that takes forever to (cross)compile:
	# man=false, gtk_doc=false, tests=false
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=plain \
		-Dman=false \
		-Dgtk_doc=false \
		-Dtests=false \
		-Dbroadway_backend=true \
		output
	ninja -C output
}

check() {
	xvfb-run ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install

	# use gtk-update-icon-cache from gtk+2.0 for now
	rm -f "$pkgdir"/usr/bin/gtk-update-icon-cache
	rm -f "$pkgdir"/usr/share/man/man1/gtk-update-icon-cache.1
}

demo() {
	pkgdesc="$pkgdesc (demonstration application)"
	install -Dm755 "$pkgdir"/usr/bin/gtk3-demo \
				   "$pkgdir"/usr/bin/gtk3-widget-factory \
				   "$pkgdir"/usr/bin/gtk3-demo-application \
				   -t "$subpkgdir"/usr/bin
	install -Dm644 "$pkgdir"/usr/share/gtk-3.0/gtkbuilder.rng \
				   -t "$subpkgdir"/usr/share/gtk-3.0
	install -Dm644 "$pkgdir"/usr/share/glib-2.0/schemas/org.gtk.Demo.gschema.xml \
				   -t "$subpkgdir"/usr/share/glib-2.0/schemas
	install -Dm644 "$pkgdir"/usr/share/applications/gtk3-widget-factory.desktop \
				   "$pkgdir"/usr/share/applications/gtk3-demo.desktop \
				   -t "$subpkgdir"/usr/share/applications
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/icons "$subpkgdir"/usr/share
}

dev() {
	replaces="gtk+-dev"
	default_dev
}

doc() {
	replaces="gtk+-doc"
	default_doc
}

sha512sums="8d2a85ad058191348927454170c8ab63dfbdfce6a7b3172824aa33b128d17b6f3de7ba85445efa658adb5d7a98d60730e014d1113b19a2f036222f32ae62bf00  gtk-21ecddab70544dc4421cd34247e9c7c2ef551409.tar.gz
e4ea76484b70bd9beb65b2964bbcff3b3f78f5f6fe70b12309a7721ca134e3735e8aaac09803f93b393a6130a703f8f346c0df89ad45d18c580dac1e0e922276  10-Revert-gdkseatdefault-Grab-touch-events-where-applic.patch
b97ccd8fb78d7c32fe91607befd6a7c0dd969fbfc9c242948fc88085133e3461583a0b18ade199b73f9659cae5f5525b940e66535a6ced4b916af9a88b3cc578  check-version.py
2464ee16e0731ad1b3a2c15cb3274bbb4ae55eab34e9c6310712003142c127efe0c342c510fe4edf399d14e33050300702432b5515954ddcb380d08aac0589c2  0001-gtk-meson.build-add-new-hdy-files.patch"
